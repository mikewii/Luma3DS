# only used to view project in QTCreator
cmake_minimum_required(VERSION 3.15)

project(arm11 LANGUAGES C ASM)
set(C_STANDARD gnu11)
set(C_STANDARD_REQUIRED ON)
set(CXX_STANDARD gnu++11)
set(CXX_STANDARD_REQUIRED ON)

set(ARCH -march=armv6k -mtune=mpcore -mfloat-abi=hard -mtp=soft)
set(CFLAGS -g -std=gnu11 -O2 -mword-relocations -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-builtin)
set(CXXFLAGS -fno-rtti -fno-exceptions -std=gnu++11)
set(WARN -Wall -Wextra -Werror -Wno-unused-value -Wno-main)
set(FALSEPOSITIVES -Wno-array-bounds -Wno-stringop-overflow -Wno-stringop-overread)

file(GLOB_RECURSE PROJECT_SOURCES CONFIGURE_DEPENDS
    "${CMAKE_CURRENT_SOURCE_DIR}/source/*c"
    "${CMAKE_CURRENT_SOURCE_DIR}/source/*cpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/source/*s")

file(GLOB_RECURSE PROJECT_HEADERS CONFIGURE_DEPENDS
    "${CMAKE_CURRENT_SOURCE_DIR}/source/*h"
    "${CMAKE_CURRENT_SOURCE_DIR}/source/*hpp"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/*h"
    "${CMAKE_CURRENT_SOURCE_DIR}/include/*hpp")

add_executable(${PROJECT_NAME} ${PROJECT_SOURCES} ${PROJECT_HEADERS})

target_include_directories(${PROJECT_NAME} PRIVATE $ENV{DEVKITPRO}/libctru/include)
target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)
target_compile_options(${PROJECT_NAME} PRIVATE ${WARN} ${FALSEPOSITIVES} ${CFLAGS} ${ARCH})

#add_custom_target(process_xml
#  COMMAND bin2o
#  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
#  COMMENT "run generated test2 in ${CMAKE_CURRENT_SOURCE_DIR}"
#  SOURCES ${test2_SOURCES}
#)
